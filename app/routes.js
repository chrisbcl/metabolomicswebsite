var done = false;
var Dataset = require('../app/models/dataset');
var User = require('../app/models/user');
var fs = require('fs');
var path = require('path');
var appDir = path.dirname(require.main.filename);

module.exports = function(app, passport, AdmZip, multer){


    var storage = multer.diskStorage({
  	destination: function (req, file, cb) {
	    var newDestination = './uploads/'+req.body.datasetName;
    	    var stat = null;
    	    try {
            	stat = fs.statSync(newDestination);
    	    } catch (err) {
        	fs.mkdirSync(newDestination);
    	    }
    	    if (stat && !stat.isDirectory()) {
         	throw new Error('Directory cannot be created because an inode of a different type exists at "' + dest + '"');
    	    }
    	    cb(null, newDestination);
     	},
      	filename: function (req, file, cb) {
    	    cb(null, file.originalname)
  	}
    })

    var upload = multer({
    	fileFilter: function(req, file, cb){
	    checkDataset(req, function(err, exists){
	        if (!exists)
		    cb(null,false);
		else cb(null, true);
	    });
	},
	storage : storage
    });    

    /*app.use(multer({ dest: './uploads/',
        rename: function(fieldname, filename){
            return filename;
        },

        onFileUploadStart: function (file){
            console.log(file.originalname + ' is starting ...')
        }, 

        onFileUploadComplete: function (file) {
            console.log(file.fieldname + ' uploaded to  ' + file.path)
        
            //zip.extractAllTo("./uploads/dataset1/", true);
         
            done = true;
        }
    }));*/


    app.post('/metabolomics/submit', upload.fields([{name: 'data'}, {name: 'metadata'}, {name: 'report'}]),
	function(req, res){
        //if (done == true){
            //var zip = new AdmZip("./uploads/"+req.files.userDataset.name);
            //zip.extractAllTo("./uploads/"+req.body.datasetName+"/",true);
            //res.end("File uploaded.");
	checkDataset(req, function(err, exists){
	 if (exists){
	    	var newDataset = Dataset();
	    	newDataset.name = req.body.datasetName;
	    	newDataset.description = req.body.datasetDescription;
	    	newDataset.userId = req.user._id;
	    	newDataset.privacy = req.body.selectprivacy;
	    	newDataset.summary = req.body.summary;
	    	newDataset.data = req.files.data[0].path;
	    	newDataset.metadata = req.files.metadata[0].path;
		var reports = [];
		for (var i = 0; i < req.files.report.length; i++){
		    reports[i] = {name: req.files.report[i].originalname, 
				  path: req.files.report[i].path}
		}
	    	newDataset.report = reports;
	    	//newDataset.userList = req.body.datasetUserList;
	    	newDataset.save();
		res.redirect('/metabolomics');
	    } else {
		req.flash('datasetMessage', "Dataset already created");
		res.redirect('/metabolomics/submit');
	    }
	});
	    //fs.mkdirSync('./uploads/'+req.body.datasetName);
	//}
    });

    app.get('/metabolomics/', function(req, res) {
	/*fs.readdir('./uploads/dataset1', function(err, files) {
	    if (err) return;
		files.forEach(function(f) {
		console.log('Files: ' + f);
	    });
	});*/

	var query = Dataset.find();
	query.select("-metadata");
	query.select("-data");
	query.select("-summary");
	query.select("-privacy");
	query.select("-report");
	query.select("-userList");
	
	query.exec(function(err, datasets){
	    if (err) return res.send(err);
	    else {
		res.render('index.ejs', {
		    user: req.user,
		    datasets: datasets
		});
	    }
	});

	/*
	Dataset.find({}, function(err, datasets){
	    if (err) return err;
	    else {
		res.render('index.ejs', {
		    user: req.user,
		    datasets: datasets,
		});
	    }
	});*/
    });

    app.get('/metabolomics/login', function(req, res){
	res.render('login.ejs', { message: req.flash('loginMessage') });
    });

    app.get('/metabolomics/submit', isLoggedIn, function(req, res){
	res.render('submit_dataset.ejs', { message: req.flash('datasetMessage')});
	console.log(req.user);
    });

    app.post('/metabolomics/login', passport.authenticate('local-login', {
        successRedirect: '/metabolomics/',
        failureRedirect: '/metabolomics/login',
        failureFlash: true
    }));

    app.get('/metabolomics/signup', function(req, res) {
	res.render('signup.ejs', {message: req.flash('signupMessage') });	
    });


    app.get('/metabolomics/profile', isLoggedIn, function(req, res){
	res.render('profile.ejs', {
	    user : req.user
	});
    });


    app.get('/metabolomics/logout', function(req, res) {
	req.logout();
	res.redirect('/metabolomics');
    });

    app.post('/metabolomics/signup', passport.authenticate('local-signup', {
	successRedirect: '/metabolomics/profile', 
	failureRedirect: '/metabolomics/signup',
	failureFlash: true
    }));

    app.get('/metabolomics/dataset/:name', function(req, res){
	Dataset.findOne({ 'name' : req.params.name}, function(err,dataset){
		if (err) return err;
		else {
		    User.findOne({ '_id': dataset.userId}, function(err, user){
			if (err) return err;
			else {
			    res.render('dataset.ejs', {
				dataset: dataset,
				user : user
			    });
			}
		    });
		}
	});

    });

    app.get('/metabolomics/data/:name', function(req, res){
	Dataset.findOne({ 'name' : req.params.name}, function(err, dataset){
	    if (err) return err;
	    else {
		var file = './' + dataset.data;
		res.download(file);
	    }
	});
    });

    app.get('/metabolomics/metadata/:name', function(req,res){
	Dataset.findOne({'name' : req.params.name}, function(err,dataset){
	    if (err) return err;
	    else {
		var file = './' + dataset.metadata;
		res.download(file);	
	    }
	});
    });

    app.get('/metabolomics/report/:name/:number', function(req, res){
	Dataset.findOne({ 'name' : req.params.name}, function(err, dataset){
	    if (err) return err;
	    else {
		var file = appDir + '/' + dataset.report[req.params.number].path;
		res.sendFile(file);
	    }
	});
    });

};
  
function isLoggedIn(req, res, next){
    if (req.isAuthenticated()){
	return next();
    }	
	
    res.redirect('/metabolomics/login');
}

function checkDataset(req, cb){
        Dataset.findOne({ name: req.body.datasetName }, function(err, dataset){
                console.log(dataset);
        if (err) res.send(401);
            if (dataset !== null) {
                cb(null, false);
            } else {
                cb(null, true);
            }
        });
    }
