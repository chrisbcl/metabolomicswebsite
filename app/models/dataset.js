// load the things we need
var mongoose = require('mongoose');

// define the schema for our user model
var datasetSchema = mongoose.Schema({
            
        name           	: String,
        description     : String,
	userId		: String,
	privacy		: String,
	summary		: String,
	data		: String,
	metadata	: String,
	report		: [{name: String, path: String}],
	userList	: [{userEmail: String}]
    
});     

        
// create the model for users and expose it to our app
module.exports = mongoose.model('Dataset', datasetSchema);
