/* Define dependencies */

var express = require("express");
var multer = require('multer');
var AdmZip = require('adm-zip');
var app = express();
var done = false;
var port = 8000;
var mongoose = require('mongoose');
var passport = require('passport');
var flash = require('connect-flash');

var morgan = require('morgan');
var cookieParser = require('cookie-parser');
var bodyParser = require('body-parser');
var session = require('express-session');

var configDB = require('./config/database.js');

/*Configure the multer */

/*app.use(multer({ dest: './uploads/',
    rename: function(fieldname, filename){
	return filename;
    },
    
    onFileUploadStart: function (file){
	console.log(file.originalname + ' is starting ...')
    }, 
   
    onFileUploadComplete: function (file) {
	console.log(file.fieldname + ' uploaded to  ' + file.path)
	
	//zip.extractAllTo("./uploads/dataset1/", true);
	
	done = true;
    }
}));*/

/*Handling routes */

/*app.get('/', function(req,res){
    res.sendfile("index.html");
});*/

/*app.post('/submit', function(req, res){
    if (done == true){
	console.log(req.user);
	var zip = new AdmZip("./uploads/"+req.files.name); 
	zip.extractAllTo("./uploads/"+req.user.local.email+"/",true);
        console.log(req.files);
	//res.end("File uploaded.");
	res.redirect('/');
    }
});*/




// configuration
mongoose.connect(configDB.url);

require('./config/passport')(passport);

// set up express application
app.use(morgan('dev'));
app.use(cookieParser());
app.use(bodyParser());

app.set('view engine', 'ejs');

// required for passport
app.use(session({ secret: 'somerandomsecret'}));
app.use(passport.initialize());
app.use(passport.session());
app.use(flash());

app.use('/metabolomics', express.static(__dirname + '/public'));

// routes 
require('./app/routes.js')(app, passport, AdmZip, multer); 

/*Run the server */
app.listen(port, function(){
    console.log("Working on port"  + port);
});
